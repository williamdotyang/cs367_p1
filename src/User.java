///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  AppStore.java
// File:             User.java
// Semester:         CS367 Fall 2015
//
// Author:           Weilan Yang, wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ////////////////////
//
// Pair Partner:     (name of your pair programming partner)
// Email:            (email address of your programming partner)
// CS Login:         (partner's login name)
// Lecturer's Name:  (name of your partner's lecturer)
// Lab Section:      (your partner's lab section number)
//
//////////////////// STUDENTS WHO GET HELP FROM OTHER THAN THEIR PARTNER //////
//                   fully acknowledge and credit all sources of help,
//                   other than Instructors and TAs.
//
// Persons:          Identify persons by name, relationship to you, and email.
//                   Describe in detail the the ideas and help they provided.
//
// Online sources:   avoid web searches to solve your problems, but if you do
//                   search, be sure to include Web URLs and description of 
//                   of any information you find.
//////////////////////////// 80 columns wide //////////////////////////////////
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

/**
 * The User class represents a single user registered at the app store. 
 * It provides a simple interface to store and retrieve all the properties 
 * associated with a user.
 *
 * <p>Bugs: (a list of bugs and other problems)
 *
 * @author Weilan Yang
 */
public class User {

	//private data members
	private String email;
	private String password;
	private String firstName;
	private String lastName;
	private String coutry;
	private String type;
	private List<App> downloaded;
	private List<App> uploaded;
	private static List<User> allUsers = new ArrayList<User>();
	
	//constructor
	public User(String email, String password, String firstName,
			String lastName, String country, String type)
			throws IllegalArgumentException {
		//null argument handling
		if (email == null || password == null || firstName == null ||
				lastName == null || country == null || type == null) {
			throw new IllegalArgumentException("null passed in parameters");
		}
		
		Iterator<User> userItr = User.allUsers.iterator();
		while (userItr.hasNext()) {
			User user = userItr.next();
			if (user.email.equals(email)) {
				throw new IllegalArgumentException(email 
						+ " has already registered an account.");
			}
		}
		
		this.email = email;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.coutry = country;
		this.type = type;
		this.downloaded = new ArrayList<App>();
		this.uploaded = new ArrayList<App>();
		User.allUsers.add(this);
	}
	
	/**
	 * Private field retriever.
	 *
	 * @return email
	 */
	public String getEmail() {
		return this.email;
	}
	
	/**
	 * Test if the given string matches with the user password. 
	 *
	 * @param testPassword 
	 * @return true if the password matches the password set for this user.
	 */
	public boolean verifyPassword(String testPassword) {
		return testPassword.equals(this.password);
	}
	
	/**
	 * Private field retriever.
	 *
	 * @return firstName
	 */
	public String getFirstName() {
		return this.firstName;
	}
	
	/**
	 * Private field retriever.
	 *
	 * @return lastName
	 */
	public String getLastName() {
		return this.lastName;
	}
	
	/**
	 * Private field retriever.
	 *
	 * @return country
	 */
	public String getCountry() {
		return this.coutry;
	}
	
	/**
	 * Test if a user is a developer.
	 *
	 * @return true if this user is a developer.
	 */
	public boolean isDeveloper() {
		return this.type.equals("developer");
	}
	
	/**
	 * Change a user type to developer.
	 *
	 */
	public void subscribeAsDeveloper() {
		this.type = "developer";
	}
	
	/**
	 * Downloads an app and adds it to the downloaded list of apps for 
	 * this user.
	 *
	 * @param app An app to be downloaded.
	 */
	public void download(App app) { 
		this.downloaded.add(app);
	}
	
	/**
	 * Uploads an app and adds it to the uploaded list of apps for this user.
	 *
	 * @param app An app to be uploaded.
	 */
	public void upload(App app) {
		this.uploaded.add(app);
	}
	
	/**
	 * Returns a list of apps installed by a user.
	 *
	 * @return A list of apps installed by a user.
	 */
	public List<App> getAllDownloadedApps() {
		return this.downloaded;
	}
	
	/**
	 * Returns a list of apps uploaded by a user.
	 *
	 * @return A list of apps uploaded by a user.
	 */
	public List<App> getAllUploadedApps() {
		return this.uploaded;
	}
		
}


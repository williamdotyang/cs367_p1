///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  AppStore.java
// File:             AppStoreDB.java
// Semester:         CS367 Fall 2015
//
// Author:           Weilan Yang, wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ////////////////////
//
// Pair Partner:     (name of your pair programming partner)
// Email:            (email address of your programming partner)
// CS Login:         (partner's login name)
// Lecturer's Name:  (name of your partner's lecturer)
// Lab Section:      (your partner's lab section number)
//
//////////////////// STUDENTS WHO GET HELP FROM OTHER THAN THEIR PARTNER //////
//                   fully acknowledge and credit all sources of help,
//                   other than Instructors and TAs.
//
// Persons:          Identify persons by name, relationship to you, and email.
//                   Describe in detail the the ideas and help they provided.
//
// Online sources:   avoid web searches to solve your problems, but if you do
//                   search, be sure to include Web URLs and description of 
//                   of any information you find.
//////////////////////////// 80 columns wide //////////////////////////////////
import java.util.List;

public class AppStoreDB {
	
	//TODO add private data members

	public AppStoreDB() {
		//TODO Remove this exception and implement the constructor
		throw new RuntimeException("constructor not implemented.");
	}

	public User addUser(String email, String password, String firstName,
			String lastName, String country, String type)
			throws IllegalArgumentException {
		
		//TODO Remove this exception and implement the method
		throw new RuntimeException("addUser() not implemented.");
	}
	
	public void addCategory(String category) {
		//TODO Remove this exception and implement the method
		throw new RuntimeException("addCategory() not implemented.");
	}
	
	public List<String> getCategories() {
		//TODO Remove this exception and implement the method
		throw new RuntimeException("getCategories() not implemented.");		
	}
	
	public User findUserByEmail(String email) {
		//TODO Remove this exception and implement the method
		throw new RuntimeException("findUserByEmail() not implemented.");	
	}
	
	public App findAppByAppId(String appId) {
		//TODO Remove this exception and implement the method
		throw new RuntimeException("findAppByAppId() not implemented.");	
	}
	
	public User loginUser(String email, String password) {
		//TODO Remove this exception and implement the method
		throw new RuntimeException("loginUser() not implemented.");	
	}

	public App uploadApp(User uploader, String appId, String appName,
			String category, double price, 
			long timestamp) throws IllegalArgumentException {
		
		//TODO Remove this exception and implement the method
		throw new RuntimeException("uploadApp() not implemented.");	
	}
	
	public void downloadApp(User user, App app) {		
		//TODO Remove this exception and implement the method
		throw new RuntimeException("downloadApp() not implemented.");	
	}
	
	public void rateApp(User user, App app, short rating) {
		//TODO Remove this exception and implement the method
		throw new RuntimeException("rateApp() not implemented.");	
	}
	
	public boolean hasUserDownloadedApp(User user, App app) {		
		//TODO Remove this exception and implement the method
		throw new RuntimeException("hasUserDownloadedApp() not implemented.");	
	}

			
	public List<App> getTopFreeApps(String category) {
		//TODO Remove this exception and implement the method
		throw new RuntimeException("getTopFreeApps() not implemented.");	
	}
	
	public List<App> getTopPaidApps(String category) {
		//TODO Remove this exception and implement the method
		throw new RuntimeException("getTopPaidApps() not implemented.");	
	}
	
	public List<App> getMostRecentApps(String category) {
		//TODO Remove this exception and implement the method
		throw new RuntimeException("getMostRecentApps() not implemented.");	
	}
}


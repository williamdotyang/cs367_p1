///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  AppStore.java
// File:             App.java
// Semester:         CS367 Fall 2015
//
// Author:           Weilan Yang, wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ////////////////////
//
// Pair Partner:     (name of your pair programming partner)
// Email:            (email address of your programming partner)
// CS Login:         (partner's login name)
// Lecturer's Name:  (name of your partner's lecturer)
// Lab Section:      (your partner's lab section number)
//
//////////////////// STUDENTS WHO GET HELP FROM OTHER THAN THEIR PARTNER //////
//                   fully acknowledge and credit all sources of help,
//                   other than Instructors and TAs.
//
// Persons:          Identify persons by name, relationship to you, and email.
//                   Describe in detail the the ideas and help they provided.
//
// Online sources:   avoid web searches to solve your problems, but if you do
//                   search, be sure to include Web URLs and description of 
//                   of any information you find.
//////////////////////////// 80 columns wide //////////////////////////////////
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

/**
 * The App class represents a single app at the app store and holds all the 
 * attributes that are associated with a typical app. It also maintains a 
 * reference to the developer who uploaded the app and you can retrieve an 
 * app’s current average rating and total number of downloads using various 
 *
 * @author Weilan Yang
 */
public class App implements Comparable<App> {
	
	//private data members
	private User developer;
	private String appId;
	private String appName;
	private String category;
	private double price;
	private long uploadTimestamp;
	private List<User> usersDownloaded;
	private List<User> usersRated;
	private List<Short> ratings;
	private static List<App> allApps = new ArrayList<App>();
	
	//constructor
	public App(User developer, String appId, String appName, String category,
			double price, long uploadTimestamp) throws IllegalArgumentException{
		if (developer == null || appId == null || appName == null || 
				category == null) 
			throw new IllegalArgumentException("null passed in parameters");
		
		if (!developer.isDeveloper()) {
			throw new IllegalArgumentException(developer.getEmail() 
					+ " is not a developer.");
		}
		
		Iterator<App> appItr = App.allApps.iterator();
		while (appItr.hasNext()) {
			App app = appItr.next();
			if (app.appId.equals(appId) && !app.developer.equals(developer)) {
				throw new IllegalArgumentException("Someone has already used "
						+ "this appId: " + appId);
			}
		}
		
		this.developer = developer;
		this.appId = appId;
		this.appName = appName;
		this.category = category;
		this.price = price;
		this.uploadTimestamp = uploadTimestamp;
		this.usersDownloaded = new ArrayList<User>();
		this.usersRated = new ArrayList<User>();
		this.ratings = new ArrayList<Short>();
		App.allApps.add(this);
	}
	
	/**
	 * Private field retriever.
	 *
	 * @return developer
	 */
	public User getDeveloper() {
		return this.developer;
	}
	
	/**
	 * Private field retriever. 
	 * 
	 * @return appId
	 */
	public String getAppId() {
		return this.appId;
	}
	
	/**
	 * Private field retriever. 
	 * 
	 * @return appName
	 */
	public String getAppName() {
		return this.appName;
	}

	/**
	 * Private field retriever. 
	 * 
	 * @return category
	 */
	public String getCategory() {
		return this.category;
	}
	
	/**
	 * Private field retriever. 
	 * 
	 * @return price
	 */
	public double getPrice() {
		return this.price;
	}
	
	/**
	 * Private field retriever. 
	 * 
	 * @return uploadTimestamp
	 */
	public long getuploadTimestamp() {
		return this.uploadTimestamp;
	}
	
	/**
	 * Update the list of users who downloaded this app.
	 *
	 * @param user A new user who downloads this app.
	 */
	public void download(User user) {
		// first time to download
		if (!this.usersDownloaded.contains(user)) {
			this.usersDownloaded.add(user);
		}
	}
	
	/**
	 * Assigns a rating to an app by the given user. 
	 * Throws an exception, if the user has already rated the app, or the 
	 * user hasn't downloaded the app, or the rating score given is not 1-5.
	 *
	 * @param user A new user who rates this app.
	 * @param rating The rating score to be assigned.
	 */
	public void rate(User user, short rating) throws IllegalArgumentException {
		// input validation
		if (!this.usersDownloaded.contains(user)) {
			throw new IllegalArgumentException(user.getEmail() + 
					" hasn't downloaded " + this.appName);
		}
		if (this.usersRated.contains(user)) {
			throw new IllegalArgumentException(user.getEmail() + 
					" has already rated " + this.appName);
		}
		if (rating < 1 || rating > 5) {
			throw new IllegalArgumentException("Invalid rating score.");
		}
		
		this.ratings.add(rating);
		this.usersRated.add(user);
	}

	/**
	 * Return the total number of downloads for this app.
	 *
	 * @return total number of downloads for this app
	 */
	public long getTotalDownloads() {
		return this.usersDownloaded.size();
	}

	/**
	 * Returns average rating for the app. Return 0.0 only if no user 
	 * has rated this app.
	 *
	 * @return average rating for this app
	 */
	public double getAverageRating() {
		if (this.usersDownloaded.isEmpty()){
			return 0.0;
		} else {
			double rating = 0.0;
			Iterator<Short> itr = this.ratings.iterator();
			while (itr.hasNext()) {
				double rate = itr.next();
				rating += rate;
			}
			rating /= this.ratings.size();
			return rating;
		}
	}
	
	/**
	 * Returns the total revenue generated for a given app. Total revenue for 
	 * an app is the (total number of downloads * price of the app) – 30% cut 
	 * imposed by your app store.
	 *
	 * @return revenue for this app. 
	 */
	public double getRevenueForApp() {
		return this.getTotalDownloads() * this.price * 0.7;
	}

	/**
	 * Returns an app score computed by the following function: 
	 * (1) score = 0.0, if total_downloads = 0, 
	 * (2) score = avg_rating * ln(total_downloads), if total_downloads > 0
	 *
	 * @return score for this app
	 */
	public double getAppScore() {
		if (this.getTotalDownloads() == 0) {
			return 0.0;
		} else {
			return this.getAverageRating() * Math.log(getTotalDownloads());
		}
	}

	/**
	 * Provides an implementation of the compare() method for the Comparable 
	 * Interface implemented by the App class. It compares two app objects 
	 * based on their age at the app store. It returns -1 if the app object 
	 * has higher uploadTimestamp value than the compared otherApp object and 
	 * vice-versa for value 1.
	 *
	 * @param otherApp The other app to be compared with this app.
	 * @return -1 if this app is older, 1 if this app is younger
	 */
	@Override
	public int compareTo(App otherApp) {
		if (this.getuploadTimestamp() > otherApp.getuploadTimestamp()) {
			return -1;
		} else {
			return 1;
		}
	}
	
}


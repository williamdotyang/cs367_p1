

public class App_test {
	public static void main(String args[]) {
		User weilan = new User("wyang@cs.wisc.edu", "123456", "Weilan",
				"Yang", "China", "user");
		User steve = new User("sjobs@apple.com", "abcdef", "Jobs", "Steve", 
				"US", "developer");
		User mark = new User("dev@fb.com", "facebooking", "Mark", "Zuckerberg", 
				"US", "developer");
		
		App flappybird = new App(steve, "com.apple.app", "flappy bird", 
				"Games", 6.0, 1428601792000L);
		App facebook = new App(mark, "com.facebook.app", "Facebook", 
				"Social", 0.0, 1411604234000L);
		
		System.out.println("something about flappybird: ");
		System.out.println(flappybird.getAppName());
		System.out.println(flappybird.getAppId());
		System.out.println(flappybird.getCategory());
		System.out.println(flappybird.getPrice());
		System.out.println(flappybird.getDeveloper());
		System.out.println(flappybird.getuploadTimestamp());
		System.out.println("downloads: "+flappybird.getTotalDownloads());
		System.out.println("ratings: "+flappybird.getAverageRating());
		System.out.println("scores: "+flappybird.getAppScore());
		System.out.println("revenue: "+flappybird.getRevenueForApp());
		
		flappybird.download(weilan);
		flappybird.download(mark);
		//flappybird.rate(steve, (short) 1);
		flappybird.rate(weilan, (short) 1);
		//flappybird.rate(weilan, (short) 2);
		flappybird.rate(mark, (short) 2);
		System.out.println("average rating: "+flappybird.getAverageRating());
		System.out.println("downloads: "+flappybird.getTotalDownloads());
		System.out.println("revenue: "+flappybird.getRevenueForApp());
		System.out.println("scores: "+flappybird.getAppScore());
		
		System.out.println(flappybird.compareTo(facebook));
		
		App gibberish = new App(steve, "com.apple.app", "flappy bird", 
				"Games", 6.0, 1428601792000L);
		App gibberish2 = new App(weilan, "com.apple.app", "flappy bird", 
				"Games", 6.0, 1428601792000L);
		App gibberish3 = new App(mark, "com.apple.app", "flappy bird", 
				"Games", 6.0, 1428601792000L);
	}
}

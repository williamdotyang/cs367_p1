public class User_test {
	public static void main(String args[]) {
		User user = new User("wyang@cs.wisc.edu", "123456", "Weilan",
				"Yang", "China", "user");
		System.out.println(user.getLastName());
		System.out.println(user.getFirstName());
		System.out.println(user.getEmail());
		System.out.println(user.getCountry());
		System.out.println("is developer?" + user.isDeveloper());
		System.out.println("try passwd 123:" + user.verifyPassword("123"));
		System.out.println("try passwd 123456:" + user.verifyPassword("123456"));
		
		User developer = new User("sjobs@apple.com", "abcdef", "Jobs", "Steve", 
				"US", "developer");
		System.out.println(developer.getLastName());
		System.out.println(developer.getFirstName());
		System.out.println(developer.getEmail());
		System.out.println(developer.getCountry());
		System.out.println("is developer?" + developer.isDeveloper());
		
		App app = new App(developer, "com.apple.app", "flappy bird", 
				"Games", 4.0, 1428601792000L);
		
		developer.upload(app);
		user.download(app);
		
		System.out.println("user downloads: " + user.getAllDownloadedApps());
		System.out.println("user uploads" + user.getAllUploadedApps());
		System.out.println("developer downloads: " + developer.getAllDownloadedApps());
		System.out.println("developer uploads" + developer.getAllUploadedApps());
		
		user.subscribeAsDeveloper();
		System.out.println("is developer?" + user.isDeveloper());
		User user2 = new User("wyang@cs.wisc.edu", "123456", "Weilan",
				"Yang", "China", "user");
	}
}
